/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.geonames;

import org.junit.Test;
import org.ow2.weblab.geonames.impl.SPARQLSelector;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;



/**
 * Test SPARQL selector
 *
 * @author ymombrun
 */
public class SPARQLSelectorTest {



	@Test(expected = IllegalArgumentException.class)
	public void testNoQueryFile() throws Exception {
		new SPARQLSelector(new ClassPathResource("AFileThatDoesNotExist"));
	}


	@Test(expected = IllegalArgumentException.class)
	public void testNotAQueryFile() throws Exception {
		new SPARQLSelector(new FileSystemResource("pom.xml"));
	}


	@Test(expected = IllegalArgumentException.class)
	public void testAValidQueryFile() throws Exception {
		new SPARQLSelector(new FileSystemResource("src/test/resources/badSelect.rq"));
	}


	@Test(expected = IllegalArgumentException.class)
	public void testNotASelectQueryFile() throws Exception {
		new SPARQLSelector(new FileSystemResource("src/main/resources/getFeatureGraph.construct"));
	}


	@Test(expected = IllegalArgumentException.class)
	public void testAnIncompleteQueryFile1() throws Exception {
		new SPARQLSelector(new FileSystemResource("src/test/resources/incompleteSelect1.rq"));
	}


	@Test(expected = IllegalArgumentException.class)
	public void testAnIncompleteQueryFile2() throws Exception {
		new SPARQLSelector(new FileSystemResource("src/test/resources/incompleteSelect2.rq"));
	}

}
