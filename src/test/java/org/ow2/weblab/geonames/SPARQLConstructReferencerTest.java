/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.geonames;

import java.net.MalformedURLException;
import java.net.URL;

import org.junit.Before;
import org.junit.Test;
import org.ow2.weblab.geonames.impl.SPARQLConstructReferencer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;



/**
 * Test SPARQL selector
 *
 * @author ymombrun
 */
public class SPARQLConstructReferencerTest {


	private URL url;


	@Before
	public void setUp() throws MalformedURLException {
		this.url = new URL("http://localhost/valid/url/but/nothing/behind/it");
	}



	@Test(expected = IllegalArgumentException.class)
	public void testNoQueryFile() throws Exception {
		new SPARQLConstructReferencer(this.url, new ClassPathResource("AFileThatDoesNotExist"));
	}


	@Test(expected = IllegalArgumentException.class)
	public void testNotAQueryFile() throws Exception {
		new SPARQLConstructReferencer(this.url, new FileSystemResource("pom.xml"));
	}


	@Test(expected = IllegalArgumentException.class)
	public void testNotAValidQueryFile() throws Exception {
		new SPARQLConstructReferencer(this.url, new FileSystemResource("src/test/resources/badSelect.rq"));
	}


	@Test(expected = IllegalArgumentException.class)
	public void testNotASelectQueryFile() throws Exception {
		new SPARQLConstructReferencer(this.url, new FileSystemResource("src/main/resources/getEntities.select"));
	}


}
