/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.geonames;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;

import org.apache.commons.io.FileUtils;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.services.Analyser;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.geonames.impl.SPARQLConstructReferencer;
import org.ow2.weblab.geonames.impl.SPARQLSelector;
import org.ow2.weblab.geonames.service.GeonamesReferencer;
import org.springframework.core.io.FileSystemResource;

import com.hp.hpl.jena.query.QueryExecutionFactory;



/**
 * Real tests that will only be running if a sparql endpoint containing features is available
 *
 * @author ymombrun
 */
public class GeonamesReferencerTest {


	private final static URL service = toUrl("http://localhost:3030/geonamesService/sparql");


	private final Analyser analyser = new GeonamesReferencer(new SPARQLSelector(new FileSystemResource("src/main/resources/getEntities.select")), new SPARQLConstructReferencer(service,
			new FileSystemResource("src/main/resources/getFeatureGraph.construct")));


	private static URL toUrl(final String url) {
		try {
			return new URL(url);
		} catch (final MalformedURLException murle) {
			throw new IllegalArgumentException(murle);
		}
	}


	@Before
	public void setUp() {
		boolean notEmpty = false;
		try {
			notEmpty = QueryExecutionFactory.sparqlService(service.toString(), "ASK {?f a <http://www.geonames.org/ontology#Feature>}").execAsk();
		} catch (final Exception e) {
			Assume.assumeNoException(e);
		}
		Assume.assumeTrue(notEmpty);
	}


	@Test()
	public void testOnRealFolder() throws Exception {
		final File input = new File("src/test/resources/corpus");
		final File output = new File("target/corpus");
		output.mkdirs();
		final Collection<File> files = FileUtils.listFiles(input, new String[] { "xml" }, false);
		for (final File f : files) {
			final Resource res = new WebLabMarshaller(true).unmarshal(f, Resource.class);
			final ProcessArgs args = new ProcessArgs();
			args.setResource(res);
			this.analyser.process(args);
			new WebLabMarshaller().marshalResource(res, new File(output, f.getName()));
		}
	}

}
