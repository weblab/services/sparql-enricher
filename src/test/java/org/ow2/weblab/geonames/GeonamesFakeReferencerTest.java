/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.geonames;

import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.core.services.Analyser;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.core.services.analyser.ProcessReturn;
import org.ow2.weblab.geonames.impl.SPARQLSelector;
import org.ow2.weblab.geonames.model.Entity;
import org.ow2.weblab.geonames.model.IEntityReferencer;
import org.ow2.weblab.geonames.service.GeonamesReferencer;
import org.purl.dc.elements.DublinCoreAnnotator;
import org.springframework.core.io.FileSystemResource;


/**
 * @author ymombrun
 */
public class GeonamesFakeReferencerTest {


	private final Analyser analyser = new GeonamesReferencer(new SPARQLSelector(new FileSystemResource("src/main/resources/getEntities.select")),
			new IEntityReferencer() {


				@Override
				public void searchAndAnnotate(Resource res, Map<Resource, List<Entity>> entitiesPerResource) {
					// Create an annotation per found entity to check that the number of annot is coherent with the type of entities of the input
					for (Entry<Resource, List<Entity>> entry : entitiesPerResource.entrySet()) {
						for (Entity entity : entry.getValue()) {
							new WProcessingAnnotator(WebLabResourceFactory.createAndLinkAnnotation(res)).writeRefersTo(URI.create(entity.uri));
						}
					}

				}
			});


	@Test()
	public void testDoNothing() throws Exception {
		final ProcessArgs args = new ProcessArgs();
		args.setResource(WebLabResourceFactory.createResource("test", "document" + System.nanoTime(), Document.class));
		final ProcessReturn pr = this.analyser.process(args);
		Assert.assertEquals(0, pr.getResource().getAnnotation().size());
	}


	@Test(expected = InvalidParameterException.class)
	public void testEmptyRequest() throws Exception {
		this.analyser.process(new ProcessArgs());
	}


	@Test(expected = InvalidParameterException.class)
	public void testNullRequest() throws Exception {
		this.analyser.process(null);
	}


	@Test()
	public void testSimpleText() throws Exception {
		final ProcessArgs args = new ProcessArgs();
		final Text text = WebLabResourceFactory.createResource("test", "text" + System.nanoTime(), Text.class);
		text.setContent("What ever... it is not read...");
		new DublinCoreAnnotator(text).writeLanguage("en");
		final WProcessingAnnotator wpa = new WProcessingAnnotator(text);

		this.annotateEntity(wpa, "wl:goodEntity", "wl:goodSegment", "http://weblab.ow2.org/wookie#Place", "NYC");
		this.annotateEntity(wpa, "wl:badEntity", "wl:badSegment", "http://weblab.ow2.org/wookie#Person", "Washington");

		args.setResource(text);
		final ProcessReturn pr = this.analyser.process(args);
		Assert.assertEquals(2, pr.getResource().getAnnotation().size());
	}


	private void annotateEntity(final WProcessingAnnotator wpa, final String entityUri, final String segmentUri, final String type, final String label) {
		wpa.startInnerAnnotatorOn(URI.create(entityUri));
		wpa.writeLabel(label);
		wpa.writeType(URI.create(type));
		wpa.endInnerAnnotator();
		wpa.startInnerAnnotatorOn(URI.create(segmentUri));
		wpa.writeRefersTo(URI.create(entityUri));
		wpa.endInnerAnnotator();
	}

}
