/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.geonames.model;

import java.util.List;
import java.util.Map;

import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.services.InsufficientResourcesException;
import org.ow2.weblab.core.services.UnexpectedException;


/**
 * Interface to be implemented by the class in charge of reading the Resource in order to generate a Map of entities per Resource to be annotated.
 *
 * @author ymombrun
 */
public interface IEntitySelector {


	/**
	 * @param res
	 *            The Resource to look up for entities inside
	 * @return A Map organised by Resource (Resource been the one on which the description of georeferenced entities should be written (in a annotation of the
	 *         whole list of entity)) that provides a list of entity found inside and to be looked up inside Geonames.
	 *
	 * @throws InsufficientResourcesException
	 *             If a file needed is missing, a connection is not available...
	 * @throws UnexpectedException
	 *             If an exception that could not have been prevented occurs.
	 */
	public Map<Resource, List<Entity>> selectEntitiesFromResource(final Resource res) throws InsufficientResourcesException, UnexpectedException;

}
