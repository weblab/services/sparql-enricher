/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.geonames.model;

import java.util.List;
import java.util.Map;

import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.services.InsufficientResourcesException;
import org.ow2.weblab.core.services.UnexpectedException;


/**
 * Interface to be implemented by the modules in charge of searching through Geonames (using a SPARQL endpoint or using a PosGIS database for instance).
 *
 * @author ymombrun
 */
public interface IEntityReferencer {



	/**
	 * @param res
	 *            The main resource
	 * @param entitiesPerResource
	 *            A Map containing each resource to be annotated with the description of the Entity in the List.
	 * @throws InsufficientResourcesException
	 *             If a file needed is missing, a connection is not available...
	 * @throws UnexpectedException
	 *             If an exception that could not have been prevented occurs.
	 */
	public void searchAndAnnotate(final Resource res, final Map<Resource, List<Entity>> entitiesPerResource) throws UnexpectedException, InsufficientResourcesException;


}
