/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.geonames.model;


/**
 * Just a bean that describes an entity
 *
 * @author ymombrun
 */
public class Entity {


	/**
	 * The label
	 */
	public final String label;


	/**
	 * The uri
	 */
	public final String uri;


	/**
	 * The uri of the type
	 */
	public final String type;


	/**
	 * The uri of the segment
	 */
	public final String referedBy;


	/**
	 * @param label
	 *            The label
	 * @param uri
	 *            The uri
	 * @param type
	 *            The type
	 * @param referedBy
	 *            The uri of the segment
	 */
	public Entity(final String label, final String uri, final String type, final String referedBy) {
		super();
		this.label = label;
		this.uri = uri;
		this.type = type;
		this.referedBy = referedBy;
	}

}
