/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.geonames.impl;

import java.io.IOException;
import java.io.StringWriter;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.extended.factory.ExceptionFactory;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.util.PoKUtil;
import org.ow2.weblab.core.model.Annotation;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.geonames.constants.Constants;
import org.ow2.weblab.geonames.model.Entity;
import org.ow2.weblab.geonames.model.IEntityReferencer;
import org.ow2.weblab.rdf.Value;
import org.purl.dc.elements.DublinCoreAnnotator;

import com.hp.hpl.jena.query.ParameterizedSparqlString;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolutionMap;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.ResourceFactory;


/**
 * Implementation of the Referencer that loads a file that contains a parametrised SPARQL Query.
 *
 * @author ymombrun
 */
public class SPARQLConstructReferencer implements IEntityReferencer {


	private static final String URI = "uri";


	private static final String REFERED_BY = "referedBy";


	private static final String TYPE = "type";


	private static final String LABEL = "label";


	private static final String LOCALISED_LABEL = "localisedLabel";


	private static final String MAIN_RESOURCE = "mainResource";


	/**
	 * The Query Endpoint address.
	 */
	private final String queryEndpoint;


	/**
	 * The low level construct template loaded.
	 */
	private final String lowLevelContructTemplate;


	/**
	 * The top level construct template loaded.
	 */
	private final String topLevelContructTemplate;


	/**
	 *
	 */
	private final Map<String, String> prefixMappings;


	/**
	 * The logger used inside.
	 */
	private final Log log;


	/**
	 * @param queryEndpoint
	 *            The URL of the endpoint to be queries (to get enrichment)
	 * @param lowLevelConstruct
	 *            The Spring Resource (i.e. path, classpath...) to the file/stream containing the construct query template
	 */
	public SPARQLConstructReferencer(final URL queryEndpoint, final org.springframework.core.io.Resource lowLevelConstruct) {
		this(queryEndpoint, lowLevelConstruct, null, new HashMap<String, String>());
	}


	/**
	 * @param queryEndpoint
	 *            The URL of the endpoint to be queries (to get enrichment)
	 * @param lowLevelConstruct
	 *            The Spring Resource (i.e. path, classpath...) to the file/stream containing the construct query template to be applied on the triple store.
	 * @param topLevelConstruct
	 *            The Spring Resource (i.e. path, classpath...) to the file/stream containing the construct query template to be applied on the document to select entities to be enriched
	 * @param prefixMappings
	 *            A map of prefixes and namespaces to be used in the RDF generated
	 */
	public SPARQLConstructReferencer(final URL queryEndpoint, final org.springframework.core.io.Resource lowLevelConstruct, final org.springframework.core.io.Resource topLevelConstruct,
			final Map<String, String> prefixMappings) {
		this.log = LogFactory.getLog(this.getClass());
		this.queryEndpoint = queryEndpoint.toString();
		this.lowLevelContructTemplate = this.readTemplate(lowLevelConstruct);
		this.topLevelContructTemplate = this.readTemplate(topLevelConstruct);
		if (this.lowLevelContructTemplate == null && this.topLevelContructTemplate == null) {
			final String message = "<" + Constants.SERVICE_NAME + "> cannot be initialised some requested parameter were null.";
			this.log.fatal(message);
			throw new IllegalArgumentException(message);
		}
		this.prefixMappings = prefixMappings;
	}


	/**
	 * @param construct
	 *            The resource that contains the template
	 * @return The template loaded or null
	 */
	protected String readTemplate(final org.springframework.core.io.Resource construct) {
		if (construct == null) {
			return null;
		}

		final String template;
		try {
			template = IOUtils.toString(construct.getURL(), "UTF-8");
		} catch (final IOException ioe) {
			final String message = "<" + Constants.SERVICE_NAME + "> cannot be initialised. Unable to load template from " + construct + ".";
			this.log.fatal(message, ioe);
			throw new IllegalArgumentException(message, ioe);
		}
		// Validate the query
		final Query q;
		try {
			q = QueryFactory.create(template);
		} catch (final Exception e) {
			final String message = "<" + Constants.SERVICE_NAME + "> cannot be initialised. Unable to parse query loaded from " + construct + ". Template is:\n " + template;
			this.log.fatal(message, e);
			throw new IllegalArgumentException(message, e);
		}
		if (!q.isConstructType()) {
			final String message = "<" + Constants.SERVICE_NAME + "> cannot be initialised. Query should be of type CONSTRUCT. Query file is " + construct + ". Query is:\n" + q.serialize();
			this.log.fatal(message);
			throw new IllegalArgumentException(message);
		}

		return template;
	}


	@Override
	public void searchAndAnnotate(final Resource mainRes, final Map<Resource, List<Entity>> entitiesPerResource) throws UnexpectedException {

		final List<Entity> documentEntities = new LinkedList<>();
		for (final Entry<Resource, List<Entity>> entry : entitiesPerResource.entrySet()) {
			documentEntities.addAll(entry.getValue());
			if (this.lowLevelContructTemplate == null) {
				continue;
			}
			this.annotateUsingConstruct(entry.getKey(), entry.getValue(), this.lowLevelContructTemplate, mainRes.getUri());
		}

		if (this.topLevelContructTemplate != null) {
			this.annotateUsingConstruct(mainRes, documentEntities, this.topLevelContructTemplate, mainRes.getUri());
		}
	}


	/**
	 * @param mainRes
	 *            The main resource uri
	 * @param entities
	 *            The list of entities to annotate
	 * @param template
	 *            The template to be used for loading the parametrised query
	 * @param resource
	 *            The resource on which to create an annotation (might be the same than main res)
	 * @throws UnexpectedException
	 *             If an error occurred constructing the query
	 */
	protected void annotateUsingConstruct(final Resource resource, List<Entity> entities, String template, final String mainRes) throws UnexpectedException {
		final Value<String> language = new DublinCoreAnnotator(resource).readLanguage();
		final Model model = ModelFactory.createDefaultModel();
		try {
			for (final Entity entity : entities) {
				final QuerySolutionMap map = new QuerySolutionMap();
				map.add(SPARQLConstructReferencer.URI, ResourceFactory.createResource(entity.uri.toString()));
				map.add(SPARQLConstructReferencer.LABEL, ResourceFactory.createPlainLiteral(entity.label));
				map.add(SPARQLConstructReferencer.TYPE, ResourceFactory.createResource(entity.type.toString()));
				map.add(SPARQLConstructReferencer.REFERED_BY, ResourceFactory.createResource(entity.referedBy.toString()));
				if (language.hasValue()) {
					map.add(SPARQLConstructReferencer.LOCALISED_LABEL, ResourceFactory.createLangLiteral(entity.label, language.firstTypedValue()));
				}
				map.add(SPARQLConstructReferencer.MAIN_RESOURCE, ResourceFactory.createResource(mainRes));

				final Query annotateEntities = new ParameterizedSparqlString(template, map).asQuery();

				try (final QueryExecution qexec = QueryExecutionFactory.sparqlService(this.queryEndpoint, annotateEntities)) {
					qexec.execConstruct(model);
				} catch (final Exception e) {
					final String message = "<" + Constants.SERVICE_NAME + ">" + " Error processing document. An error occured executing construct query for resource " + resource.getUri();
					this.log.error(message, e);
					throw ExceptionFactory.createUnexpectedException(message, e);
				}
			}
			if (!model.isEmpty()) {
				final Annotation annot = WebLabResourceFactory.createAndLinkAnnotation(resource);
				model.setNsPrefixes(this.prefixMappings);

				final StringWriter writer = new StringWriter();
				model.write(writer, "RDF/XML");
				PoKUtil.setPoKData(annot, writer.toString().replace("\r", ""));
			}
		} finally {
			model.close();
		}
	}


}
