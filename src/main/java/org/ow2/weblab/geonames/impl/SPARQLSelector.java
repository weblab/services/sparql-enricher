/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.geonames.impl;

import java.io.IOException;
import java.io.StringReader;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.extended.util.PoKUtil;
import org.ow2.weblab.core.extended.util.ResourceUtil;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.geonames.constants.Constants;
import org.ow2.weblab.geonames.model.Entity;
import org.ow2.weblab.geonames.model.IEntitySelector;
import org.purl.dc.elements.DublinCoreAnnotator;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;


/**
 * Loads a SPARQL Query and apply it each PoK found in the received Resource.
 *
 * The output Map lists each entity per sub-resource of the main resource in order to enable annotation Resource per Resource (often Text by Text).
 *
 * @author ymombrun
 */
public class SPARQLSelector implements IEntitySelector {


	private static final String LABEL = "label";


	private static final String REFERED_BY = "referedBy";


	private static final String TYPE = "type";


	private static final String URI = "uri";


	/**
	 * Logger used inside
	 */
	protected final Log logger;


	/**
	 * The Construct query to be applied on the received resource
	 */
	protected final Query query;



	/**
	 * @param queryFile
	 *            The Spring Resource containing the text of the select query to be loaded
	 * @throws IllegalArgumentException
	 *             If query file does not contain a valid select query with expected compulsory vars
	 */
	public SPARQLSelector(final org.springframework.core.io.Resource queryFile) throws IllegalArgumentException {
		this.logger = LogFactory.getLog(this.getClass());
		try {
			this.query = QueryFactory.read(queryFile.getURL().toString());
		} catch (final IOException ioe) {
			final String message = "<" + Constants.SERVICE_NAME + "> cannot be initialised. Unable to find query file " + queryFile + ".";
			this.logger.fatal(message, ioe);
			throw new IllegalArgumentException(message, ioe);
		} catch (final Exception e) {
			final String message = "<" + Constants.SERVICE_NAME + "> cannot be initialised. Unable to parse query file " + queryFile + ".";
			this.logger.fatal(message, e);
			throw new IllegalArgumentException(message, e);
		}


		this.logger.trace("Loaded query:\n" + this.query.serialize());

		if (!this.query.isSelectType()) {
			final String message = "<" + Constants.SERVICE_NAME + "> cannot be initialised. Query should be of type SELECT. Query file is " + queryFile + ". Query is:\n" + this.query.serialize();
			this.logger.fatal(message);
			throw new IllegalArgumentException(message);
		}

		final List<String> resultVars = this.query.getResultVars();
		// Label is compulsory
		if (!resultVars.contains(SPARQLSelector.LABEL)) {
			final String message = "<" + Constants.SERVICE_NAME + "> cannot be initialised. Query must return variable named '" + SPARQLSelector.LABEL + "'. Query file is " + queryFile
					+ ". Query is:\n" + this.query.serialize();
			this.logger.fatal(message);
			throw new IllegalArgumentException(message);
		}
		// Only one of referedBy and uri is compulsory. Depending on the annotator implementation
		if (!resultVars.contains(SPARQLSelector.REFERED_BY) && !resultVars.contains(SPARQLSelector.URI)) {
			final String message = "<" + Constants.SERVICE_NAME + "> cannot be initialised. Query must return variable named '" + SPARQLSelector.REFERED_BY + "' or '" + SPARQLSelector.URI
					+ "'. Query file is " + queryFile + ". Query is:\n" + this.query.serialize();
			this.logger.fatal(message);
			throw new IllegalArgumentException(message);
		}
		// Type is optional
	}


	@Override
	public Map<Resource, List<Entity>> selectEntitiesFromResource(final Resource res) {

		// Retrieves each PoK of the main resource.
		final Map<PieceOfKnowledge, Resource> annots = ResourceUtil.getSelectedSubResourcesMap(res, PieceOfKnowledge.class);
		if (annots.isEmpty()) {
			this.logger.trace("Not a single PoK in Resource '" + res.getUri() + "'. Nothing will be done.");
			return Collections.emptyMap();
		}


		final Map<Resource, List<Entity>> entitiesPerResource = new HashMap<>();
		for (final Entry<PieceOfKnowledge, Resource> entry : annots.entrySet()) {

			final Resource innerRes = entry.getValue();
			// Creates a simple Jena Model from the content of the Annotation
			final Model model = ModelFactory.createDefaultModel();
			try (final StringReader reader = new StringReader(PoKUtil.getPoKData(entry.getKey()))) {
				model.read(reader, null, "RDF/XML");
			}

			// Execute the loaded query on it and create Entities from the resultSet
			try (final QueryExecution qexec = QueryExecutionFactory.create(this.query, model)) {
				final ResultSet rs = qexec.execSelect();
				this.parseResultSet(entitiesPerResource, innerRes, rs);
			} finally {
				model.close();
			}
		}

		return entitiesPerResource;
	}


	/**
	 * @param entitiesPerResource
	 *            A Map of list of entities per resource (often Text MediaUnits)
	 * @param innerRes
	 *            The current resource associated with the result set
	 * @param rs
	 *            The result set to parse
	 */
	protected void parseResultSet(final Map<Resource, List<Entity>> entitiesPerResource, final Resource innerRes, final ResultSet rs) {
		if (rs.hasNext()) {
			final String source = new DublinCoreAnnotator(innerRes).readSource().firstTypedValue();
			final String resourceUri = innerRes.getUri();
			while (rs.hasNext()) {
				final QuerySolution solution = rs.nextSolution();
				if (!solution.contains(SPARQLSelector.LABEL)) {
					this.logger.warn("<" + Constants.SERVICE_NAME + ">" + " Unable to properly process document '<" + resourceUri + ">' " + " - '<" + source + ">'. "
							+ "Answered solution does not contain " + SPARQLSelector.LABEL + " " + solution.toString() + ".");
					continue;
				}
				final String uri = this.readVarURI(solution, SPARQLSelector.URI);
				final String referedBy = this.readVarURI(solution, SPARQLSelector.REFERED_BY);

				if ((uri == null) && (referedBy == null)) {
					this.logger.warn("<" + Constants.SERVICE_NAME + ">" + " Unable to properly process document '<" + resourceUri + ">' " + " - '<" + source + ">'. "
							+ "Answered solution does not contain " + SPARQLSelector.URI + " nor " + SPARQLSelector.REFERED_BY + " " + solution.toString() + ".");
					continue;
				}

				final String type = this.readVarURI(solution, SPARQLSelector.TYPE);
				final String label = solution.getLiteral(SPARQLSelector.LABEL).getLexicalForm();
				final Entity entity = new Entity(label, uri, type, referedBy);
				if (!entitiesPerResource.containsKey(innerRes)) {
					entitiesPerResource.put(innerRes, new LinkedList<Entity>());
				}
				entitiesPerResource.get(innerRes).add(entity);
			}
		}
	}


	protected String readVarURI(final QuerySolution solution, final String var) {
		final String uri;
		if (solution.contains(var)) {
			uri = solution.get(var).asResource().getURI();
		} else {
			uri = null;
		}
		return uri;
	}

}
