/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.geonames.service;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.extended.factory.ExceptionFactory;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.services.Analyser;
import org.ow2.weblab.core.services.InsufficientResourcesException;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.core.services.analyser.ProcessReturn;
import org.ow2.weblab.geonames.constants.Constants;
import org.ow2.weblab.geonames.model.Entity;
import org.ow2.weblab.geonames.model.IEntityReferencer;
import org.ow2.weblab.geonames.model.IEntitySelector;
import org.purl.dc.elements.DublinCoreAnnotator;


/**
 * Simple implementation of the Geonames Referencer.
 * It uses two inner components implementations.
 * One is in charge of selecting inside the Resource which entity should be annotated.
 * And one other is charge of creating the Annotation that describes the relation between existing entities and Geonames Features.
 *
 * @author ymombrun
 */
public class GeonamesReferencer implements Analyser {


	/**
	 * The logger used inside.
	 */
	private final Log log;


	/**
	 * The entity selector in charge of reading the resource to generate list of Entities.
	 */
	private final IEntitySelector entitySelector;


	/**
	 * The entity referencer in charge of calling Geonames and annotate the resource.
	 */
	private final IEntityReferencer entityReferencer;



	/**
	 * @param entitySelector
	 *            Selector of entities in the Resource
	 * @param entityReferencer
	 *            Referencer of entities
	 */
	public GeonamesReferencer(final IEntitySelector entitySelector, final IEntityReferencer entityReferencer) {
		this.log = LogFactory.getLog(this.getClass());
		this.entityReferencer = entityReferencer;
		this.entitySelector = entitySelector;
		this.log.info(
				"<" + Constants.SERVICE_NAME + "> started with selector " + entitySelector.getClass().getSimpleName() + " and referencer " + this.entityReferencer.getClass().getSimpleName() + ".");
	}


	@Override
	public ProcessReturn process(final ProcessArgs args) throws InvalidParameterException, UnexpectedException, InsufficientResourcesException {
		final Resource res = this.checkArgs(args);

		final String source = new DublinCoreAnnotator(res).readSource().firstTypedValue();
		this.log.debug("<" + Constants.SERVICE_NAME + ">" + " Start processing document " + "'<" + res.getUri() + ">'" + " - " + "'<" + source + ">'");

		// Retrieves entities in the Resource
		final Map<Resource, List<Entity>> entitiesPerResource = this.entitySelector.selectEntitiesFromResource(res);

		// Annotate the resource with references
		this.entityReferencer.searchAndAnnotate(res, entitiesPerResource);

		this.log.debug("<" + Constants.SERVICE_NAME + ">" + " End processing document " + "'<" + res.getUri() + ">'" + " - " + "'<" + source + ">'");
		final ProcessReturn pr = new ProcessReturn();
		pr.setResource(res);
		return pr;
	}


	/**
	 * @param args
	 *            The check Args to be checked
	 * @return The resource inside check args
	 * @throws InvalidParameterException
	 *             If the process args does not contain a valid resource with an URI
	 */
	protected Resource checkArgs(final ProcessArgs args) throws InvalidParameterException {
		if (args == null) {
			final String message = "<" + Constants.SERVICE_NAME + "> Unable to properly process document. ProcessArgs is null.";
			this.log.error(message);
			throw ExceptionFactory.createInvalidParameterException(message);
		}
		final Resource res = args.getResource();
		if (res == null) {
			final String message = "<" + Constants.SERVICE_NAME + "> Unable to properly process document. Resource in ProcessArgs is null.";
			this.log.error(message);
			throw ExceptionFactory.createInvalidParameterException(message);
		}
		if (res.getUri() == null) {
			final String message = "<" + Constants.SERVICE_NAME + "> Unable to properly process document. Resource in ProcessArgs has no URI and it is compulsory.";
			this.log.error(message);
			throw ExceptionFactory.createInvalidParameterException(message);
		}
		return res;
	}

}
