# SPARQL Enricher service

This project is a Maven based Java project of a Web Service.
This WebLab WebService was originaly in charge of requesting a triple store containing Geonames extract to retrieve the geonames:Feature (along with some properties) associated to a named entity already annotated in the Document.
It has been renamed to SPARQL Enricher service has be find out that we were able to use it with other kind of knowledge base to enrich documents.


# Build status
## Master
[![build status](https://gitlab.ow2.org/weblab/services/sparql-enricher/badges/master/build.svg)](https://gitlab.ow2.org/weblab/services/sparql-enricher/commits/master)
## Develop
[![build status](https://gitlab.ow2.org/weblab/services/sparql-enricher/badges/develop/build.svg)](https://gitlab.ow2.org/weblab/services/sparql-enricher/commits/develop)

# How to build it

In order to be build it, you need to have access to the Maven dependencies it is using. Most of the dependencies are in the central repository and thus does not implies specific configuration.
However, the WebLab Core dependencies are not yet included in the Maven central repository but in a dedicated one that we manage ourselves.
Thus you may have to add the repositories that are listed in the settings.xml. 
